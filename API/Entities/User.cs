﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace API.Entities
{
    public class User
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string FirstName { get; set; } = string.Empty;

        [Required]
        [StringLength(50)]
        public string LastName { get; set; } = string.Empty;

        [Required]
        [StringLength(80)]
        public string Email { get; set; } = string.Empty;

        [Required]
        [StringLength(80)]
        public string Password { get; set; } = string.Empty;

        public string MobileNumber { get; set; } = string.Empty;

        [Required]
        public DateTime CreatedOn { get; set; }

        public UserType UserType { get; set; } = UserType.NONE;
        
        public AccountStatus AccountStatus { get; set; } = AccountStatus.UNAPROOVED;
    }
}
